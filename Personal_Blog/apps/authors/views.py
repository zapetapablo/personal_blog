
from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
#our apps
from .models import Post


class blog_DetailView(DetailView):
    template_name = "blog/base_blog.html"
    model = Post
    context_object_name = "posts"

    def get_context_data(self, **kwargs):
        context = super(blog_DetailView, self).get_context_data(**kwargs)
        posts = Post.objects.get(id = self.kwargs['pk'])
        return context


class blog_TemplateView(TemplateView):
    template_name = "blog/base_blog.html"


class Blog_ListView(ListView):
    model = Post
    template_name = "blog/list_blogs.html"
    context_object_name = "List"
    paginate_by = 4
