from django.db import models

#third apps
from ckeditor.fields import RichTextField

class Author(models.Model):
    """Model definition for Author."""
    name = models.CharField(('Nombres'), max_length=75)
    last_name = models.CharField(('Apellidos'), max_length=75)
    avatar = models.CharField(('Avatar'), max_length=50)
    description = models.CharField('Descripción', max_length=250)

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'

    def __str__(self):
        return "{}".format(self.avatar)

class Post(models.Model):
    title = models.CharField(("titulo"), max_length=150)
    post_date = models.DateField("Fecha de creacion", auto_now=False, auto_now_add=True)
    content = RichTextField()
    Reference_image = models.ImageField("Imagen de referencia", upload_to='media/refernce')
    post_status = models.BooleanField(default=False)
    name = models.ForeignKey(Author, on_delete=models.CASCADE)
    

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return "{} {} {} ".format(self.id, self.title, self.post_status)
