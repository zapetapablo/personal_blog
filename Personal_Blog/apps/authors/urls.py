from django.urls import path, re_path, include
from . import views

app_name = "author_urls"

urlpatterns = [
    path(
        'Blog/<pk>',
        views.blog_DetailView.as_view(),
        name="base_Blog"
        ),
    path(
        'listar_Blogs/',
        views.Blog_ListView.as_view(),
        name="list_Blog"
        ),
    
]