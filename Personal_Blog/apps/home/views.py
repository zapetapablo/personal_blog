
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from django.urls import reverse

from .forms import Contact_Form


from django.views.generic import (
    TemplateView,
    DetailView,
    ListView
    )


class Home_templateView(TemplateView):
    template_name = "home/home.html"


class Contact_templateView(FormView):
    template_name = "home/contact.html"
    form_class = Contact_Form
    success_url = 'home/success.html'

    def form_valid(self, form):
        from_email = form.cleaned_data['email']
        message = form.cleaned_data['message'] + "Mensaje enviado de:" + " " + from_email 
        subject = form.cleaned_data['subject']  

        try:
            send_mail(subject, message, "zapeta.pablo@gmail.com", ["pablozapetalop@gmail.com"])
        except BadHeaderError:
            return HttpResponse('Header Invalido encontrado!!.')
        return HttpResponseRedirect(
            reverse(
                'home_urls:success'
            )
        )


class About_templateView(TemplateView):
    template_name = "home/about.html"



class Success_TemplateView(TemplateView):
    template_name = "home/success.html"

