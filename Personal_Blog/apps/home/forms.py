from django import forms

class Contact_Form(forms.Form):
    email = forms.EmailField(required=True)
    subject = forms.CharField(max_length=30, required=False)
    message = forms.CharField(max_length=255, required=True)