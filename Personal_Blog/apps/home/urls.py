from django.urls import path
from . import views

app_name = "home_urls"

urlpatterns = [
    path(
        '',
        views.Home_templateView.as_view(),
        name="home"),
    path(
        'contacto/',
        views.Contact_templateView.as_view(),
        name="contact"),
    path(
        'Acerca_de_nosotros/',
        views.About_templateView.as_view(),
        name="About_us"),
    path(
        'Exito/',
        views.Success_TemplateView.as_view(),
        name="success"),
    
]
